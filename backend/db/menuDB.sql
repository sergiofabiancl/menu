CREATE TABLE "users" (
  "id" SERIAL UNIQUE PRIMARY KEY,
  "full_name" varchar,
  "username" varchar UNIQUE,
  "email" varchar,
  "birth" timestamp,
  "created_at" timestamp,
  "lang" int,
  "country_code" int,
  "pass" varchar,
  "roles" int
);

CREATE TABLE "countries" (
  "code" int PRIMARY KEY,
  "name" varchar,
  "continent_name" varchar
);

CREATE TABLE "lang" (
  "code" int PRIMARY KEY,
  "name" varchar
);

CREATE TABLE "role" (
  "code" int PRIMARY KEY,
  "name" varchar,
  "description" varchar
);

CREATE TABLE "user_role" (
  "id" int,
  "code" int,
  PRIMARY KEY ("id", "code")
);

ALTER TABLE "users" ADD FOREIGN KEY ("country_code") REFERENCES "countries" ("code");

ALTER TABLE "users" ADD FOREIGN KEY ("lang") REFERENCES "lang" ("code");

ALTER TABLE "user_role" ADD FOREIGN KEY ("id") REFERENCES "users" ("id");

ALTER TABLE "user_role" ADD FOREIGN KEY ("code") REFERENCES "role" ("code");

